
    <!-- Navigation -->
    <nav id="mainNav" class="navbar fixed-top navbar-toggleable-md navbar-inverse bg-inverse">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarExample" aria-controls="navbarExample" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <?php if(!isset($_SESSION['logged_in'])){ ?>
        <a class="navbar-brand" href="<?php echo site_url('Home'); ?>" >
			<img src="<?php echo base_url('assets/img/LOGO RSHS.png'); ?>" height="30" class="d-inline-block align-top" alt="">
		</a>
		<?php } else {?>
		<a class="navbar-brand" href="<?php echo site_url('Page'); ?>" >
			<img src="<?php echo base_url('assets/img/LOGO RSHS.png'); ?>" height="30" class="d-inline-block align-top" alt="">
		</a>
        <div class="collapse navbar-collapse" id="navbarExample">
            <ul class="navbar-nav ml-auto">
                 <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle mr-lg-2" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-user"></i> </i> <?php echo $this->session->kategori_user?> <b class="caret"></b>
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="#"><i class="fa fa-fw fa-user"></i> <?php echo $this->session->nama_user?></a>
                        <a class="dropdown-item" href="<?php echo site_url('Page/logout'); ?>"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                    </div>
                </li>
                <!-- <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fa fa-fw fa-sign-out"></i> Logout</a>
                </li> -->
            </ul>
        </div>
        <?php }?>
    </nav>
