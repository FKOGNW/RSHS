<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>

<head>
	<title>RSHS</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Bootstrap core CSS -->
	<link href="<?php echo base_url("assets/vendor/bootstrap/css/bootstrap.min.css");?>" rel="stylesheet">

	<!-- Custom fonts for this template -->
	<link href="<?php echo base_url("assets/vendor/font-awesome/css/font-awesome.min.css");?>" rel="stylesheet" type="text/css">

	<!-- Plugin CSS -->
	<link href="<?php echo base_url("assets/vendor/datatables/dataTables.bootstrap4.css");?>" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="<?php echo base_url("assets/css/sb-admin.css");?>" rel="stylesheet">
</head>

<body id="page-top">
<?php include('partials/navbar.php'); ?>

	<div class="content-wrapper py-3">

        <?php include('partials/sidebar.php'); ?>
        <div class="container-fluid pt-5" style="padding-left: 250px">
            <div class="row">

                <div class="col">


			<!-- Example Tables Card -->
			<div class="card mb-3">
				<div class="card-header">
					<i class="fa fa-table"></i> Data Verifikasi Hapus Alat Medis RSUP dr.Hasan Sadikin
				</div>
				<div class="card-block">
					<div class="table-responsive">
						<?php 
							$status= 0;
							foreach ($dataMedis as $alat) {
								if($alat->status_rekomendasi== "ajukan hapus")
									$status= 1;
							}
						?>
						<?php if($status== 1){?>	
						<table class="table table-bordered" width="100%" id="dataTable" cellspacing="0">
							<thead>
								<tr>
									<th>No</th>
									<th>Nama User</th>
								</tr>
							</thead>
							<tbody>
								<?php $no=1;
									$tempUser= "0";
								 	foreach ($dataMedis as $alat) {
								?>
								<?php
								 	if($alat->status_rekomendasi=="ajukan hapus" && $tempUser!= $alat->nama_petugas){
									$tempUser= $alat->nama_petugas;
								 ?>
								<tr>
									<td><?php echo $no++; ?></td>
									<td><?php echo $alat->nama_petugas; ?></td>
									<?php If($this->session->kategori_user== "verifikator"){?>
									<td>
										<a href="<?php echo site_url('Page/verifikasiHapusMedis'); ?>" class="btn btn-primary btn-xs" role="button" onclick="myFunction()">Verifikasi penghapusan</a>

										<script>
											function myFunction() {
											    alert("Verifikasi penghapusan berhasil");
											}
										</script>
									</td>
									<?php }?>
								</tr>
								<?php } }?>
							</tbody>
						</table>
						<?php } else {?>
						<p>Tidak ada yang terverifikasi</p>
						<?php } ?>
					</div>
				</div>
			</div>
	<!-- Example Tables Card -->
			<div class="card mb-3">
				<div class="card-header">
					<i class="fa fa-table"></i> Data Verifikasi Perencanaan Alat Medis RSUP dr.Hasan Sadikin
				</div>
				<div class="card-block">
					<div class="table-responsive">
						<?php 
							$status= 0;
							foreach ($dataMedis as $alat) {
								if($alat->status_rekomendasi== "rekomendasi ajukan")
									$status= 1;
							}
						?>
						<?php if($status== 1){?>	
						<table class="table table-bordered" width="100%" id="dataTable" cellspacing="0">
							<thead>
								<tr>
									<th>No</th>
									<th>Nama User</th>
								</tr>
							</thead>
							<tbody>
								<?php $no=1;
									$tempUser= "0";
								 	foreach ($dataMedis as $alat) {
								?>
								<?php
								 	if($alat->status_rekomendasi=="rekomendasi ajukan" && $tempUser!= $alat->nama_petugas){
									$tempUser= $alat->nama_petugas;
								 ?>
								<tr>
									<td><?php echo $no++; ?></td>
									<td><?php echo $alat->nama_petugas; ?></td>
									<?php If($this->session->kategori_user== "verifikator"){?>
									<td>
										<a href="#" class="btn btn-primary btn-xs" role="button" onclick="myFunction()">Verifikasi perencanaan</a>

										<script>
											function myFunction() {
											    alert("Verifikasi perencanaan berhasil");
											}
										</script>
									</td>
									<?php }?>
								</tr>
								<?php } }?>
							</tbody>
						</table>
						<?php } else {?>
						<p>Tidak ada yang terverifikasi</p>
						<?php } ?>
					</div>
				</div>
			</div>


			<!-- Example Tables Card -->
			<div class="card mb-3">
				<div class="card-header">
					<i class="fa fa-table"></i> Data Verifikasi Hapus Alat Non Medis RSUP dr.Hasan Sadikin
				</div>
				<div class="card-block">
					<div class="table-responsive">
						<?php 
							$status= 0;
							foreach ($dataNonMedis as $alat) {
								if($alat->status_rekomendasi== "ajukan hapus")
									$status= 1;
							}
						?>
						<?php if($status== 1){?>	
						<table class="table table-bordered" width="100%" id="dataTable" cellspacing="0">
							<thead>
								<tr>
									<th>No</th>
									<th>Nama User</th>
								</tr>
							</thead>
							<tbody>
								<?php $no=1;
									$tempUser= "0";
								 	foreach ($dataNonMedis as $alat) {
								?>
								<?php
								 	if($alat->status_rekomendasi=="ajukan hapus" && $tempUser!= $alat->nama_petugas){
									$tempUser= $alat->nama_petugas;
								 ?>
								<tr>
									<td><?php echo $no++; ?></td>
									<td><?php echo $alat->nama_petugas; ?></td>
									<?php If($this->session->kategori_user== "verifikator"){?>
									<td>
										<a href="<?php echo site_url('Page/verifikasiHapusNonMedis'); ?>" class="btn btn-primary btn-xs" role="button" onclick="myFunction()">Verifikasi penghapusan</a>

										<script>
											function myFunction() {
											    alert("Verifikasi penghapusan berhasil");
											}
										</script>
									</td>
									<?php }?>
								</tr>
								<?php } }?>
							</tbody>
						</table>
						<?php } else {?>
						<p>Tidak ada yang terverifikasi</p>
						<?php } ?>
					</div>
				</div>
			</div>
			<!-- Example Tables Card -->
			<div class="card mb-3">
				<div class="card-header">
					<i class="fa fa-table"></i> Data Verifikasi Perencanaan Alat Non Medis RSUP dr.Hasan Sadikin
				</div>
				<div class="card-block">
					<div class="table-responsive">
						<?php 
							$status= 0;
							foreach ($dataNonMedis as $alat) {
								if($alat->status_rekomendasi== "rekomendasi ajukan")
									$status= 1;
							}
						?>
						<?php if($status== 1){?>	
						<table class="table table-bordered" width="100%" id="dataTable" cellspacing="0">
							<thead>
								<tr>
									<th>No</th>
									<th>Nama User</th>
								</tr>
							</thead>
							<tbody>
								<?php $no=1;
									$tempUser= "0";
								 	foreach ($dataNonMedis as $alat) {
								?>
								<?php
								 	if($alat->status_rekomendasi=="rekomendasi ajukan" && $tempUser!= $alat->nama_petugas){
									$tempUser= $alat->nama_petugas;
								 ?>
								<tr>
									<td><?php echo $no++; ?></td>
									<td><?php echo $alat->nama_petugas; ?></td>
									<?php If($this->session->kategori_user== "verifikator"){?>
									<td>
										<a href="#" class="btn btn-primary btn-xs" role="button" onclick="myFunction()">Verifikasi perencanaan</a>

										<script>
											function myFunction() {
											    alert("Verifikasi perencanaan berhasil");
											}
										</script>
									</td>
									<?php }?>
								</tr>
								<?php } }?>
							</tbody>
						</table>
						<?php } else {?>
						<p>Tidak ada yang terverifikasi</p>
						<?php } ?>
					</div>
				</div>
			</div>

				</div>
			</div>
		</div>
		<!-- /.container-fluid -->

	</div>
	<!-- /.content-wrapper -->
	<a class="scroll-to-top rounded" href="#page-top">
		<i class="fa fa-chevron-up"></i>
	</a>

	<!-- Bootstrap core JavaScript -->
	<script src="<?php echo base_url("assets/vendor/jquery/jquery.min.js");?>"></script>
	<script src="<?php echo base_url("assets/vendor/tether/tether.min.js");?>"></script>
	<script src="<?php echo base_url("assets/vendor/bootstrap/js/bootstrap.min.js");?>"></script>

	<!-- Plugin JavaScript -->
	<script src="<?php echo base_url("vendor/jquery-easing/jquery.easing.min.js");?>"></script>
	<script src="<?php echo base_url("vendor/chart.js/Chart.min.js");?>"></script>
	<script src="<?php echo base_url("vendor/datatables/jquery.dataTables.js");?>"></script>
	<script src="<?php echo base_url("vendor/datatables/dataTables.bootstrap4.js");?>"></script>

	<!-- Custom scripts for this template -->
	<script src="<?php echo base_url("js/sb-admin.min.js");?>"></script>

</body>

</html>
