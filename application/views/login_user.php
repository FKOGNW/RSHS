<!DOCTYPE html>
<html>
<head>
  <title>Login User</title>
      <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url("assets/vendor/bootstrap/css/bootstrap.min.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/style_login.css");?>" rel="stylesheet">
        <!-- Custom fonts for this template -->
    <link href="<?php echo base_url("assets/vendor/font-awesome/css/font-awesome.min.css");?>" rel="stylesheet" type="text/css">
</head>
<body>
<div style="background: grey;">
	<div class="container py-3" >
		<div class="row" >
			<div class="col-sm-2 text-right">
				<img src="<?php echo base_url("assets/img/polban.png") ?>" style = "height: 100px;">
			</div>
			<div class="col-sm-8">
				<p class="h2 text-center">Subsistem Informasi Gudang Pengembalian Alat Medis dan Nonmedis RSUP dr.Hasan Sadikin Bandung</p>
			</div>
			<div class="col-sm-2">
				<img src="<?php echo base_url("assets/img/LOGO RSHS.png") ?>" class="align-middle" style = "width: 150px;">
			</div>
		</div>
	</div>
</div>
<center>
	
	<div class="container">    
	        
	    <div id="loginbox" class="mainbox col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3"> 
	        
	        <div class="row">                
	            <div class="iconmelon">
	              <img height="150" width="150" class="pull-left" src="<?php echo base_url("assets/img/user.png"); ?>" alt="User"> 
	            </div>
	        </div>
	        
	        <div class="panel panel-default" >
	            <div class="panel-heading">
	                <div class="panel-title text-center">User</div>
	            </div>     

	            <div class="panel-body" >

	                <form action="<?php echo site_url('Home/processLoginUser'); ?>" name="form" id="form" class="form-horizontal" enctype="multipart/form-data" method="post">
	                   
	                    <div class="input-group">
	                        <input id="user" type="text" class="form-control" name="username" value="" placeholder="Username" tabindex="1">                                        
	                    </div>

	                    <div class="input-group">
	                        <input id="password" type="password" class="form-control" name="password" placeholder="Password" tabindex="1">
	                    </div>                                                                  

	                    <div class="form-group">
	                        <!-- Button -->
	                        <div class="col-sm-12 controls">
	                            <button type="submit" href="#" class="btn btn-primary"><i class="glyphicon glyphicon-log-in"></i> Log in</button>                          
	                        </div>
	                    </div>

	                </form>     

	            </div>                     
	        </div>  
	    </div>
	</div>

</center>
	<div id="particles"></div>
</body>
</html>


