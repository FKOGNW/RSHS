<!DOCTYPE html>
<html>
<head>
	<title>Welcome</title>
	<link href="<?php echo base_url("assets/vendor/bootstrap/css/bootstrap.min.css");?>" rel="stylesheet">
	<link href="<?php echo base_url("assets/css/style_omb.css");?>" rel="stylesheet">
	<link href="<?php echo base_url("assets/vendor/font-awesome/css/font-awesome.min.css");?>" rel="stylesheet" type="text/css">

</head>
<body>
<div style="background: grey;">
	<div class="container py-3" >
		<div class="row" >
			<div class="col-sm-2 text-right">
				<img src="<?php echo base_url("assets/img/polban.png") ?>" style = "height: 100px;">
			</div>
			<div class="col-sm-8">
				<p class="h2 text-center">Subsistem Informasi Gudang Pengembalian Alat Medis dan Nonmedis RSUP dr.Hasan Sadikin Bandung</p>
			</div>
			<div class="col-sm-2">
				<img src="<?php echo base_url("assets/img/LOGO RSHS.png") ?>" class="align-middle" style = "width: 150px;">
			</div>
		</div>
	</div>
</div>
<div class="container">
    <div class="omb_login mt-5 pt-5">
		<div class="row justify-content-center omb_socialButtons">
    	    <div class="col-xs-4 col-sm-3 text-center">
			    <img src="<?php echo base_url("assets/img/user.png") ?>" style = "height: 150px;" >    
		        <a href="<?php echo site_url('Home/loginUser'); ?>" class="btn btn-lg btn-block omb_btn-facebook">
			        <span class="hidden-xs">User</span>
		        </a>
	        </div>
        	<div class="col-xs-4 col-sm-3 text-center">
		        <img src="<?php echo base_url("assets/img/admin.png") ?>" style = "height: 150px;">
		        <a href="<?php echo site_url('Home/loginAdmin'); ?>" class="btn btn-lg btn-block omb_btn-twitter">
			        <span class="hidden-xs">Admin</span>
		        </a>
	        </div>	
        	<div class="col-xs-4 col-sm-3 text-center">
			    <img src="<?php echo base_url("assets/img/verifikator.png") ?>" style = "height: 150px;">
		        <a href="<?php echo site_url('Home/loginVerifikator'); ?>" class="btn btn-lg btn-block omb_btn-google">
			        <span class="hidden-xs">Verifikator</span>
		        </a>
	        </div>	
		</div>
	</div>
</div>	
</body>
</html>
