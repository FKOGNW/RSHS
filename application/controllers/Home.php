<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Home extends CI_Controller {

		public function index()
		{
			
			$this->session->sess_destroy();
			$this->load->view('home');
		}


		public function loginUser(){
			if(!$this->session->logged_in) {
				$this->load->view('login_user');
			}else{
				redirect(site_url('Page'));
			}
		}

		public function processLoginUser(){
			$data['username'] = $this->input->post('username');
			$data['password'] = md5($this->input->post('password'));
			$now = new DateTime();
			$now->setTimezone(new DateTimezone('Asia/Jakarta'));
			$data['last_login']= $now->format('Y-m-d H:i:s');
			// $data['last login'] = date('Y-m-d H:i:s', now());

			$result = $this->User_model->loginUser($data);
			$this->User_model->lastLogin($data);
			$this->session->set_userdata("logged_in", true);
			$this->session->set_userdata("nama_user", $this->User_model->getNama($data));
			$this->session->set_userdata("kategori_user", $this->User_model->getKategori($data));
			if ($result == true) {
				redirect(site_url('Page'));
				$this->session->set_userdata("logged_in", true);
				$this->session->set_userdata("kategori_user", $this->User_model->getKategori($data));
				$this->session->set_userdata("nama_user", $this->User_model->getNama($data));
			} else {
				$this->session->sess_destroy();
				redirect(site_url('Home/loginUser'));
			}
		}

		public function loginAdmin(){
			if(!$this->session->logged_in) {
				$this->load->view('login_admin');
			}else{
				redirect(site_url('Page'));
			}
		}

		public function processLoginAdmin(){
			$data['username'] = $this->input->post('username');
			$data['password'] = md5($this->input->post('password'));
			$now = new DateTime();
			$now->setTimezone(new DateTimezone('Asia/Jakarta'));
			$data['last_login']= $now->format('Y-m-d H:i:s');
			// $data['last login'] = date('Y-m-d H:i:s', now());

			$result = $this->User_model->loginAdmin($data);
			$this->User_model->lastLogin($data);
			$this->session->set_userdata("logged_in", true);
			$this->session->set_userdata("nama_user", $this->User_model->getNama($data));
			$this->session->set_userdata("kategori_user", $this->User_model->getKategori($data));
			if ($result == true) {
				redirect(site_url('Page'));
				$this->session->set_userdata("logged_in", true);
				$this->session->set_userdata("kategori_user", $this->User_model->getKategori($data));
				$this->session->set_userdata("nama_user", $this->User_model->getNama($data));
			} else {
				$this->session->sess_destroy();
				redirect(site_url('Home/loginAdmin'));
			}
		}

		public function loginVerifikator(){
			if(!$this->session->logged_in) {
				$this->load->view('login_verifikator');
			}else{
				redirect(site_url('Page'));
			}
		}

		public function processLoginVerifikator(){
			$data['username'] = $this->input->post('username');
			$data['password'] = md5($this->input->post('password'));
			$now = new DateTime();
			$now->setTimezone(new DateTimezone('Asia/Jakarta'));
			$data['last_login']= $now->format('Y-m-d H:i:s');
			// $data['last login'] = date('Y-m-d H:i:s', now());

			$result = $this->User_model->loginVerifikator($data);
			$this->User_model->lastLogin($data);
			$this->session->set_userdata("logged_in", true);
			$this->session->set_userdata("nama_user", $this->User_model->getNama($data));
			$this->session->set_userdata("kategori_user", $this->User_model->getKategori($data));
			if ($result == true) {
				redirect(site_url('Page'));
				$this->session->set_userdata("logged_in", true);
				$this->session->set_userdata("kategori_user", $this->User_model->getKategori($data));
				$this->session->set_userdata("nama_user", $this->User_model->getNama($data));
			} else {
				$this->session->sess_destroy();
				redirect(site_url('Home/loginVerifikator'));
			}
		}

		// public function logout() {
		// 	$this->session->sess_destroy();
		// 	redirect(site_url('page'));
		// }
	}
?>
