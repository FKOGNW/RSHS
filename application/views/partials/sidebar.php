<div style="width: 250px; position: fixed; z-index: 10;" class="sidebar pt-5 px-2">
    <ul class="sidebar-nav navbar-nav">
        <li class="nav-item active">
            <a class="nav-link" href="<?php echo site_url('Page'); ?>"><i class="fa fa-fw fa-dashboard"></i> Beranda</a>
        </li>
        <li class="nav-item">
            <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMulti"><i class="fa fa-fw fa-sitemap"></i> Data Pengembalian</a>
            <ul class="sidebar-second-level collapse" id="collapseMulti">
                <li>
                    <a href="<?php echo site_url('Page/pageAlatMedis'); ?>">Alat Medis</a>
                </li>
                <li>
                    <a href="<?php echo site_url('Page/pageAlatNonMedis'); ?>">Alat Nonmedis</a>
                </li>
            </ul>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('Page/pageMutasi'); ?>"><i class="fa fa-fw fa-table"></i> Data Mutasi</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('Page/pageRekomendasiHapus'); ?>"><i class="fa fa-fw fa-table"></i> Rekomendasi Penghapusan</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('Page/pageRekomendasiAjukan') ?>"><i class="fa fa-fw fa-table"></i> Rekomendasi Perencanaan</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('Page/pageVerifikasi'); ?>"><i class="fa fa-fw fa-table"></i> Verifikasi</a>
        </li>
    </ul>
</div>
    
