<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Page extends CI_Controller {

		public function index()
		{
			$this->load->view('page');
		}
		public function logout() {
			$this->session->sess_destroy();
			redirect(site_url('Home'));
		}

		public function pageMutasi(){
			$data['dataMutasi']=$this->Alat_model->selectAllMutasi()->result();
			$this->load->view('page_mutasi', $data);
		}
		public function pageAlatMedis(){
			$data['dataMedis']=$this->Alat_model->selectAllMedis()->result();
			$this->load->view('page_pengembalian_medis', $data);
		}
		public function pageAlatNonMedis(){
			$data['dataNonmedis']=$this->Alat_model->selectAllNonmedis()->result();
			$this->load->view('page_pengembalian_nonmedis', $data);
		}
		public function pageInputMutasi(){
			$this->load->view('page_input_mutasi');
		}
		public function pageInputMedis(){
			$this->load->view('page_input_medis');
		}
		public function pageInputNonMedis(){
			$this->load->view('page_input_nonmedis');
		}
		public function pageRekomendasiHapus(){
			$data['dataMedis']=$this->Alat_model->selectAllMedis()->result();
			$data['dataNonMedis']=$this->Alat_model->selectAllNonmedis()->result();
			$this->load->view('page_rekomendasi_penghapusan', $data);
		}
		public function pageRekomendasiAjukan(){
			$data['dataMedis']=$this->Alat_model->selectAllMedis()->result();
			$data['dataNonMedis']=$this->Alat_model->selectAllNonmedis()->result();
			$this->load->view('page_rekomendasi_pengajuan', $data);
		}
		public function pageVerifikasi(){
			$data['dataMedis']=$this->Alat_model->selectAllMedis()->result();
			$data['dataNonMedis']=$this->Alat_model->selectAllNonmedis()->result();
			$this->load->view('page_verifikasi', $data);
		}
		public function pageEditMutasi($id){
			$data['dataMutasi']= $this->Alat_model->selectByIdMutasi($id)->row();
			$this->load->view('page_edit_mutasi', $data);
		}
		public function pageEditMedis($id){
			$data['dataMedis']= $this->Alat_model->selectByIdMedis($id)->row();
			$this->load->view('page_edit_medis', $data);
		}
		public function pageEditNonmedis($id){
			$data['dataNonMedis']= $this->Alat_model->selectByIdNonMedis($id)->row();
			$this->load->view('page_edit_nonmedis', $data);
		}

		public function processInputDataMutasi(){
			$data['nama_barang']= $this->input->post('nama_barang');
			$data['merk']= $this->input->post('merk');
			$data['jumlah']= $this->input->post('jumlah');
			$data['nup']= $this->input->post('nup');
			$data['dari_ruangan']= $this->input->post('dari_ruangan');
			$data['ke_ruangan']= $this->input->post('ke_ruangan');
			$data['tanggal']= $this->input->post('tanggal');
			$data['keterangan']= $this->input->post('keterangan');
			$data['kategori']= $this->input->post('kategori');


			$this->Alat_model->insertDataMutasi($data);

			redirect(site_url('Page/pageMutasi'));
		}

		public function processInputDataMedis(){
			$data['nama_barang']= $this->input->post('nama_barang');
			$data['merk']= $this->input->post('merk');
			$data['jumlah']= $this->input->post('jumlah');
			$data['NUP']= $this->input->post('nup');
			$data['ruangan']= $this->input->post('ruangan');
			$data['tahun_perolehan']= $this->input->post('tahun_perolehan');
			$data['harga_perolehan']= $this->input->post('harga_perolehan');
			$data['tanggal_pengembalian']= $this->input->post('tanggal_pengembalian');
			$data['kondisi']= $this->input->post('kondisi');
			$data['keterangan']= $this->input->post('keterangan');


			$this->Alat_model->insertDataMedis($data);

			redirect(site_url('Page/pageAlatMedis'));
		}

		public function processInputDataNonMedis(){
			$data['nama_barang']= $this->input->post('nama_barang');
			$data['merk']= $this->input->post('merk');
			$data['jumlah']= $this->input->post('jumlah');
			$data['NUP']= $this->input->post('nup');
			$data['ruangan']= $this->input->post('ruangan');
			$data['tahun_perolehan']= $this->input->post('tahun_perolehan');
			$data['harga_perolehan']= $this->input->post('harga_perolehan');
			$data['tanggal_pengembalian']= $this->input->post('tanggal_pengembalian');
			$data['kondisi']= $this->input->post('kondisi');
			$data['keterangan']= $this->input->post('keterangan');


			$this->Alat_model->insertDataNonmedis($data);

			redirect(site_url('Page/pageAlatNonMedis'));
		}

		public function processEditDataMutasi(){
			$data['nama_barang']= $this->input->post('nama_barang');
			$data['merk']= $this->input->post('merk');
			$data['jumlah']= $this->input->post('jumlah');
			$data['nup']= $this->input->post('nup');
			$data['dari_ruangan']= $this->input->post('dari_ruangan');
			$data['ke_ruangan']= $this->input->post('ke_ruangan');
			$data['tanggal']= $this->input->post('tanggal');
			$data['keterangan']= $this->input->post('keterangan');
			$data['kategori']= $this->input->post('kategori');
			$id= $this->input->post('id');


			$this->Alat_model->updateDataMutasi($id, $data);

			redirect(site_url('Page/pageMutasi'));
		}

		public function processEditDataMedis(){
			$data['nama_barang']= $this->input->post('nama_barang');
			$data['merk']= $this->input->post('merk');
			$data['jumlah']= $this->input->post('jumlah');
			$data['NUP']= $this->input->post('nup');
			$data['ruangan']= $this->input->post('ruangan');
			$data['tahun_perolehan']= $this->input->post('tahun_perolehan');
			$data['harga_perolehan']= $this->input->post('harga_perolehan');
			$data['tanggal_pengembalian']= $this->input->post('tanggal_pengembalian');
			$data['kondisi']= $this->input->post('kondisi');
			$data['keterangan']= $this->input->post('keterangan');
			$id= $this->input->post('id');


			$this->Alat_model->updateDataMedis($id, $data);

			redirect(site_url('Page/pageAlatMedis'));
		}

		public function processEditDataNonMedis(){
			$data['nama_barang']= $this->input->post('nama_barang');
			$data['merk']= $this->input->post('merk');
			$data['jumlah']= $this->input->post('jumlah');
			$data['NUP']= $this->input->post('nup');
			$data['ruangan']= $this->input->post('ruangan');
			$data['tahun_perolehan']= $this->input->post('tahun_perolehan');
			$data['harga_perolehan']= $this->input->post('harga_perolehan');
			$data['tanggal_pengembalian']= $this->input->post('tanggal_pengembalian');
			$data['kondisi']= $this->input->post('kondisi');
			$data['keterangan']= $this->input->post('keterangan');
			$id= $this->input->post('id');


			$this->Alat_model->updateDataNonmedis($id, $data);

			redirect(site_url('Page/pageAlatNonMedis'));
		}

		public function deleteDataMutasi($id){
			$this->Alat_model->deleteDataMutasi($id);

			redirect(site_url('Page/pageMutasi'));
		}

		public function deleteDataMedis($id){
			$this->Alat_model->deleteDataMedis($id);

			redirect(site_url('Page/pageAlatMedis'));
		}

		public function deleteDataNonMedis($id){
			$this->Alat_model->deleteDataNonmedis($id);

			redirect(site_url('Page/pageAlatNonMedis'));
		}

		public function rekomendasiHapusMedis($id, $petugas){
			$this->Alat_model->rekomendasiHapusMedis($id, $petugas);
			redirect(site_url('Page/pageAlatMedis'));
		}

		public function rekomendasiHapusNonMedis($id, $petugas){
			$this->Alat_model->rekomendasiHapusNonMedis($id, $petugas);
			redirect(site_url('Page/pageAlatNonMedis'));
		}
		
		public function verifikasiHapusMedis(){
			$this->Alat_model->verifikasiHapusMedis();
			redirect(site_url('Page/pageVerifikasi'));
		}
		
		public function verifikasiHapusNonMedis(){
			$this->Alat_model->verifikasiHapusNonMedis();
			redirect(site_url('Page/pageVerifikasi'));
		}

		public function ajukanHapusMedis(){
			$this->Alat_model->ajukanHapusMedis();
			redirect(site_url('Page/pageRekomendasiHapus'));
		}

		public function ajukanHapusNonMedis(){
			$this->Alat_model->ajukanHapusNonMedis();
			redirect(site_url('Page/pageRekomendasiHapus'));
		}



		public function rekomendasiAjukanMedis($id, $petugas){
			$this->Alat_model->rekomendasiAjukanMedis($id, $petugas);
			redirect(site_url('Page/pageAlatMedis'));
		}

		public function rekomendasiAjukanNonMedis($id, $petugas){
			$this->Alat_model->rekomendasiAjukanNonMedis($id, $petugas);
			redirect(site_url('Page/pageAlatNonMedis'));
		}
		
		public function verifikasiAjukanMedis(){
			$this->Alat_model->verifikasiAjukanMedis();
			redirect(site_url('Page/pageVerifikasi'));
		}
		
		public function verifikasiAjukanNonMedis(){
			$this->Alat_model->verifikasiAjukanNonMedis();
			redirect(site_url('Page/pageVerifikasi'));
		}

		public function ajukanPengajuanMedis(){
			$this->Alat_model->ajukanAjukanMedis();
			redirect(site_url('Page/pageRekomendasiAjukan'));
		}

		public function ajukanPengajuanNonMedis(){
			$this->Alat_model->ajukanAjukanNonMedis();
			redirect(site_url('Page/pageRekomendasiAjukan'));
		}


		public function search(){
			$keyword = $this->input->post('keyword');
			$data['dataMutasi']=   $this->Alat_model->search($keyword);
			$this->load->view('page_mutasi', $data);
		}


		public function searchPengMed(){
			$keyword = $this->input->post('keyword');
			$kategori = $this->input->post('kategori');
			$data['dataMedis']=   $this->Alat_model->searchAlatMedis($keyword, $kategori);
			$this->load->view('page_pengembalian_medis', $data);
		}
		public function searchPengNonMed(){
			$keyword = $this->input->post('keyword');
			$kategori = $this->input->post('kategori');
			$data['dataNonmedis']=   $this->Alat_model->searchAlatNonMedis($keyword, $kategori);
			$this->load->view('page_pengembalian_nonmedis', $data);
		}
	}
?>
