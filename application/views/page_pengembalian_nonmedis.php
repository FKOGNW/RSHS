<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>

<head>
	<title>RSHS</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Bootstrap core CSS -->
	<link href="<?php echo base_url("assets/vendor/bootstrap/css/bootstrap.min.css");?>" rel="stylesheet">

	<!-- Custom fonts for this template -->
	<link href="<?php echo base_url("assets/vendor/font-awesome/css/font-awesome.min.css");?>" rel="stylesheet" type="text/css">

	<!-- Plugin CSS -->
	<link href="<?php echo base_url("assets/vendor/datatables/dataTables.bootstrap4.css");?>" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="<?php echo base_url("assets/css/sb-admin.css");?>" rel="stylesheet">
</head>

<body id="page-top">
<?php include('partials/navbar.php'); ?>

	<div class="content-wrapper py-3">

        <?php include('partials/sidebar.php'); ?>
        <div class="container-fluid pt-5" style="padding-left: 250px">
            <div class="row">

                <div class="col">


					<!-- Example Tables Card -->
					<div class="card mb-3">
						<div class="card-header">
							<h5>Cari</h5>
							<form id="contact" action="<?php echo site_url('Page/searchPengNonMed'); ?>" method="post">
								<div class="form-group row">
									<label for="searchType" class="col-sm-4 col-form-label">Berdasarkan</label>
									<div class="col-sm-8">
										<select name="kategori" id="searchType" class="form-control">
											<option value="ruangan" >Ruangan</option>
											<option value="kondisi">Kondisi</option>
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label for="Keyword" class="col-sm-4 col-form-label">Keyword</label>
									<div class="col-sm-8">
										<input id="Keyword" name = "keyword" type="text" placeholder="Search here..." tabindex="1" required autofocus class="form-control">
									</div>
								</div>
								<button class="btn btn-secondary" name="submit" value="tambah" type="submit" id="contact-submit" data-submit="...Sending">Search</button>
							</form>
						</div>
						<div class="card-header">
							<i class="fa fa-table"></i> Data Pengembalian Alat Non Medis RSUP dr.Hasan Sadikin
						</div>
						<div class="card-block">
							<div class="table-responsive">
								<table class="table table-bordered" width="100%" id="dataTable" cellspacing="0">
									<thead>
										<tr>
											<th>No</th>
											<th>Nama barang</th>
											<th>Merk</th>
											<th>Jumlah</th>
											<th>NUP</th>
											<th>Ruangan</th>
											<th>Tahun perolehan</th>
											<th>Harga perolehan</th>
											<th>Tanggal pengembalian</th>
											<th>Kondisi</th>
											<th>Keterangan</th>
											<?php if($this->session->kategori_user== "administrator"){?>
											<th></th>
											<th></th>
											<?php }?>
										</tr>
									</thead>
									<tbody>
										<?php $no=1; foreach ($dataNonmedis as $alat) { ?>
										<?php if($alat->status_rekomendasi!="terhapus" && $alat->status_rekomendasi!="rekomendasi penghapusan"){?>
										<tr>
											<td><?php echo $no++; ?></td>
											<td><?php echo $alat->nama_barang; ?></td>
											<td><?php echo $alat->merk; ?></td>
											<td><?php echo $alat->jumlah; ?></td>
											<td><?php echo $alat->NUP; ?></td>
											<td><?php echo $alat->ruangan; ?></td>
											<td><?php echo $alat->tahun_perolehan; ?></td>
											<td><?php echo $alat->harga_perolehan; ?></td>
											<td><?php echo $alat->tanggal_pengembalian; ?></td>
											<td><?php echo $alat->kondisi; ?></td>
											<td><?php echo $alat->keterangan; ?></td>
											<?php if($this->session->kategori_user== "administrator"){?>
											<td>
												<a href="<?php echo site_url('Page/pageEditNonMedis/'.$alat->ID); ?>" class="btn btn-info btn-xs" role="button">Edit</a>
											</td>
											<td>
												<a href="<?php echo site_url('Page/deleteDataNonMedis/'.$alat->ID); ?>" class="btn btn-danger btn-xs" role="button">Delete</a>
												<a href="<?php echo site_url('Page/rekomendasiHapusNonMedis/'.$alat->ID. "/". $this->session->nama_user); ?>" class="btn btn-danger btn-xs" role="button" onclick="myFunction()">Rekomendasi Hapus</a>

												<script>
													function myFunction() {
													    alert("Rekomendasi hapus berhasil");
													}
												</script>
											</td>
											<?php } else If($this->session->kategori_user== "user"){?>
											<td>
												<a href="<?php echo site_url('Page/rekomendasiAjukanNonMedis/'.$alat->ID. "/" . $this->session->nama_user); ?>" class="btn btn-success btn-xs" role="button" onclick="myFunction2()">Rekomendasi Perencanaan</a>

												<script>
													function myFunction2() {
													    alert("Rekomendasi perencanaan berhasil");
													}
												</script>
											</td>
											<?php }?>
										</tr>
										<?php } }?>
									</tbody>
								</table>
							</div>
						</div>

				<div class="card-footer small text-muted">
					<?php if($this->session->kategori_user== "administrator"){?>
					<a href="<?php echo site_url('Page/pageInputNonMedis'); ?>" class="btn btn-primary btn-xs" role="button">Input</a> &nbsp
					<a href="#" class="btn btn-success btn-xs" role="button">Print</a> &nbsp
					<a href="#" class="btn btn-warning btn-xs" role="button" onclick="myFunction()">Help</a> &nbsp

					<script>
						function myFunction() {
						    alert("Hubungi Admin");
						}
					</script>
					<?php }?>
				</div>
					</div>

				</div>
			</div>
		</div>
		<!-- /.container-fluid -->

	</div>
	<!-- /.content-wrapper -->
	<a class="scroll-to-top rounded" href="#page-top">
		<i class="fa fa-chevron-up"></i>
	</a>

	<!-- Bootstrap core JavaScript -->
	<script src="<?php echo base_url("assets/vendor/jquery/jquery.min.js");?>"></script>
	<script src="<?php echo base_url("assets/vendor/tether/tether.min.js");?>"></script>
	<script src="<?php echo base_url("assets/vendor/bootstrap/js/bootstrap.min.js");?>"></script>

	<!-- Plugin JavaScript -->
	<script src="<?php echo base_url("vendor/jquery-easing/jquery.easing.min.js");?>"></script>
	<script src="<?php echo base_url("vendor/chart.js/Chart.min.js");?>"></script>
	<script src="<?php echo base_url("vendor/datatables/jquery.dataTables.js");?>"></script>
	<script src="<?php echo base_url("vendor/datatables/dataTables.bootstrap4.js");?>"></script>

	<!-- Custom scripts for this template -->
	<script src="<?php echo base_url("js/sb-admin.min.js");?>"></script>

</body>

</html>
