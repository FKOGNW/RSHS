<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function loginUser($data) {

		$condition = "username =" . "'" . $data['username'] . "' AND " . "password =" . "'" . $data['password'] . "' AND " . "kategori = ". "'". "user" . "'";
		$this->db->from('tbuser');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return true;
		} else {
			return false;
		}
	}

	public function loginAdmin($data){
		$condition = "username =" . "'" . $data['username'] . "' AND " . "password =" . "'" . $data['password'] . "' AND " . "kategori = ". "'". "administrator" . "'";
		$this->db->select('*');
		$this->db->from('tbuser');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return true;
		} else {
			return false;
		}
	}

	public function loginVerifikator($data){
		$condition = "username =" . "'" . $data['username'] . "' AND " . "password =" . "'" . $data['password'] . "' AND " . "kategori = ". "'". "verifikator" . "'";
		$this->db->select('*');
		$this->db->from('tbuser');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return true;
		} else {
			return false;
		}
	}


	public function getNama($data){
		$condition = "username =" . "'" . $data['username'] . "' AND " . "password =" . "'" . $data['password']."'" ;
		$this->db->from('tbuser');
		$this->db->where($condition);
		return $this->db->get()->row()->username;
	}

	public function getKategori($data){
		$condition = "username =" . "'" . $data['username'] . "' AND " . "password =" . "'" . $data['password'] ."'";
		$this->db->from('tbuser');
		$this->db->where($condition);
		return $this->db->get()->row()->kategori;
	}

	public function lastLogin($data){
		$this->db->where('username', $data['username']);
	    $this->db->set('last_login', $data['last_login']);
	    $this->db->update('tbuser');
	}


}
