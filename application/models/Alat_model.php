<?php

	/*
	*	Alat_model.php
	* 	Model untuk tbalat
	*/

	class Alat_model extends CI_Model
	{

		function __construct()
		{
			parent::__construct();
		}


		/*data mutasi alat medis dan non medis*/
		function selectAllMutasi()
		{
			$this->db->select('*');
			$this->db->from('tbalat');
			$this->db->order_by('ID', 'ASC');
			return $this->db->get();
		}

		function insertDataMutasi($data){
			$this->db->insert('tbalat', $data);
		}

		function deleteDataMutasi($id){
			$this->db->where('ID', $id);
			$this->db->delete('tbalat');
		}
		function selectByIdMutasi($id)
		{
			$this->db->select('*');
			$this->db->from('tbalat');
			$this->db->where('ID',$id);
			return $this->db->get();
		}	
		function updateDataMutasi($id, $data)
		{
			$this->db->where('ID',$id);
			$this->db->update('tbalat',$data);
		}


		/*data pengembalian alat medis*/	
		function selectAllMedis()
		{
			$this->db->select('*');
			$this->db->from('tpengmed');
			$this->db->order_by('ID', 'ASC');
			return $this->db->get();
		}

		function insertDataMedis($data){
			$this->db->insert('tpengmed', $data);
		}

		function deleteDataMedis($id){
			$this->db->where('ID', $id);
			$this->db->delete('tpengmed');
		}
		function selectByIdMedis($id)
		{
			$this->db->select('*');
			$this->db->from('tpengmed');
			$this->db->where('ID',$id);
			return $this->db->get();
		}	
		function updateDataMedis($id, $data)
		{
			$this->db->where('ID',$id);
			$this->db->update('tpengmed',$data);
		}


		/*data pengembalian alat nonmedis*/	
		function selectAllNonmedis()
		{
			$this->db->select('*');
			$this->db->from('tpengnonmed');
			$this->db->order_by('ID', 'ASC');
			return $this->db->get();
		}

		function insertDataNonmedis($data){
			$this->db->insert('tpengnonmed', $data);
		}

		function deleteDataNonmedis($id){
			$this->db->where('ID', $id);
			$this->db->delete('tpengnonmed');
		}
		function selectByIdNonmedis($id)
		{
			$this->db->select('*');
			$this->db->from('tpengnonmed');
			$this->db->where('ID',$id);
			return $this->db->get();
		}	
		function updateDataNonmedis($id, $data)
		{
			$this->db->where('ID',$id);
			$this->db->update('tpengnonmed',$data);
		}

		function rekomendasiHapusMedis($id, $petugas){
	    	$this->db->where('ID', $id);
	    	$this->db->set('status_rekomendasi', 'rekomendasi hapus');
	    	$this->db->set('nama_petugas', $petugas);
	    	$this->db->update('tpengmed');
	    }


		function rekomendasiHapusNonMedis($id, $petugas){
	    	$this->db->where('ID', $id);
	    	$this->db->set('status_rekomendasi', 'rekomendasi hapus');
	    	$this->db->set('nama_petugas', $petugas);
	    	$this->db->update('tpengnonmed');
	    }

		function verifikasiHapusMedis($id){
	    	$this->db->where('status_rekomendasi', 'ajukan hapus');
	    	$this->db->set('status_rekomendasi', 'terhapus');
	    	$this->db->update('tpengmed');
	    }

		function verifikasiHapusNonMedis($id){
	    	$this->db->where('status_rekomendasi', 'ajukan hapus');
	    	$this->db->set('status_rekomendasi', 'terhapus');
	    	$this->db->update('tpengnonmed');
	    }

	    function ajukanHapusMedis(){
	    	$this->db->where('status_rekomendasi', 'rekomendasi hapus');
	    	$this->db->set('status_rekomendasi', 'ajukan hapus');
	    	$this->db->update('tpengmed');
	    }

	    function ajukanHapusNonMedis(){
	    	$this->db->where('status_rekomendasi', 'rekomendasi hapus');
	    	$this->db->set('status_rekomendasi', 'ajukan hapus');
	    	$this->db->update('tpengnonmed');
	    }


		function rekomendasiAjukanMedis($id, $petugas){
	    	$this->db->where('ID', $id);
	    	$this->db->set('status_rekomendasi', 'rekomendasi ajukan');
	    	$this->db->set('nama_petugas', $petugas);
	    	$this->db->update('tpengmed');
	    }


		function rekomendasiAjukanNonMedis($id, $petugas){
	    	$this->db->where('ID', $id);
	    	$this->db->set('status_rekomendasi', 'rekomendasi ajukan');
	    	$this->db->set('nama_petugas', $petugas);
	    	$this->db->update('tpengnonmed');
	    }

		// function verifikasiAjukanMedis($id){
	 //    	$this->db->where('status_rekomendasi', 'ajukan hapus');
	 //    	$this->db->set('status_rekomendasi', 'terhapus');
	 //    	$this->db->update('tpengmed');
	 //    }

		// function verifikasiAjukanNonMedis($id){
	 //    	$this->db->where('status_rekomendasi', 'ajukan hapus');
	 //    	$this->db->set('status_rekomendasi', 'terhapus');
	 //    	$this->db->update('tpengnonmed');
	 //    }

	    function ajukanAjukanMedis(){
	    	$this->db->where('status_rekomendasi', 'rekomendasi ajukan');
	    	$this->db->set('status_rekomendasi', 'ajukan pengajuan');
	    	$this->db->update('tpengmed');
	    }

	    function ajukanAjukanNonMedis(){
	    	$this->db->where('status_rekomendasi', 'rekomendasi ajukan');
	    	$this->db->set('status_rekomendasi', 'ajukan pengajuan');
	    	$this->db->update('tpengnonmed');
	    }

	    /*fungsi searching*/
		public function search($keyword)
	    {
	    	// $this->db->where("kota LIKE '%$keyword%'");
	    	$this->db->like('kategori', $keyword, 'after');
	    	$query= $this->db->get('tbalat');
	        return $query->result();
	    }

	    /*fungsi searching*/
		public function searchAlatMedis($keyword, $kategori)
	    {
	    	// $this->db->where("kota LIKE '%$keyword%'");
	    	$this->db->like($kategori, $keyword, 'before');
	    	$query= $this->db->get('tpengmed');
	        return $query->result();
	    }
	    /*fungsi searching*/
		public function searchAlatNonMedis($keyword, $kategori)
	    {
	    	// $this->db->where("kota LIKE '%$keyword%'");
	    	$this->db->like($kategori, $keyword, 'before');
	    	$query= $this->db->get('tpengnonmed');
	        return $query->result();
	    }			

	}

?>
