<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>

<head>
	<title>RSHS</title>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url("assets/vendor/bootstrap/css/bootstrap.min.css");?>" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="<?php echo base_url("assets/vendor/font-awesome/css/font-awesome.min.css");?>" rel="stylesheet" type="text/css">

    <!-- Plugin CSS -->
    <link href="<?php echo base_url("assets/vendor/datatables/dataTables.bootstrap4.css");?>" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url("assets/css/sb-admin.css");?>" rel="stylesheet">
</head>

<body id="page-top">
<?php include('partials/navbar.php'); ?>

    <div class="content-wrapper py-3">

        <?php include('partials/sidebar.php'); ?>
        <div class="container-fluid pt-5" style="padding-left: 250px">
            <div class="row">

                <div class="col">

                    <!-- Example Tables Card -->
                    <div class="card mb-25">
                        <div class="card-block">
                            <div class="jumbotron text-center">
                                <h3>Subsistem Informasi Gudang Pengembalian Alat Medis dan Nonmedis</h3>
                                <br>
                                <br> 
                                <img src="<?php echo base_url('assets/img/LOGO RSHS.png'); ?>" width = "200" >
                                <br>
                                <br>
                                <p style="color: blue" class="h4">RSUP dr.Hasan Sadikin</p>
                                
                                <br>
                                <br>
                                <p>Jalan Pasteur No.38, Kota Bandung, Jawa Barat 40161 Indonesia</p>
                                <p>Telp.+6222 255 5111, Fax. +6222 203 2216</p>
                                <p>Email : rsup@rshs.web.id</p>
                                <p>www.rshs.or.id</p> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /.content-wrapper -->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fa fa-chevron-up"></i>
    </a>

    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo base_url("assets/vendor/jquery/jquery.min.js");?>"></script>
    <script src="<?php echo base_url("assets/vendor/tether/tether.min.js");?>"></script>
    <script src="<?php echo base_url("assets/vendor/bootstrap/js/bootstrap.min.js");?>"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url("vendor/jquery-easing/jquery.easing.min.js");?>"></script>
    <script src="<?php echo base_url("vendor/chart.js/Chart.min.js");?>"></script>
    <script src="<?php echo base_url("vendor/datatables/jquery.dataTables.js");?>"></script>
    <script src="<?php echo base_url("vendor/datatables/dataTables.bootstrap4.js");?>"></script>

    <!-- Custom scripts for this template -->
    <script src="<?php echo base_url("js/sb-admin.min.js");?>"></script>

</body>

</html>
