<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>

<head>
	<title>RSHS</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Bootstrap core CSS -->
	<link href="<?php echo base_url("assets/vendor/bootstrap/css/bootstrap.min.css");?>" rel="stylesheet">

	<!-- Custom fonts for this template -->
	<link href="<?php echo base_url("assets/vendor/font-awesome/css/font-awesome.min.css");?>" rel="stylesheet" type="text/css">

	<!-- Plugin CSS -->
	<link href="<?php echo base_url("assets/vendor/datatables/dataTables.bootstrap4.css");?>" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="<?php echo base_url("assets/css/sb-admin.css");?>" rel="stylesheet">
</head>

<body id="page-top">
<?php include('partials/navbar.php'); ?>

	<div class="content-wrapper py-3">

        <?php include('partials/sidebar.php'); ?>
        <div class="container-fluid pt-5" style="padding-left: 250px">
            <div class="row">

                <div class="col">


			<!-- Example Tables Card -->
			<div class="card mb-25">
				<div class="card-header">
					<i class="fa fa-edit"></i> Form Edit Data Mutasi
				</div>
				<div class="card-block">
					<form class="form-horizontal" action="<?php echo site_url('Page/processEditDataMutasi'); ?>" method="post">
					    <div class="form-group">
					      <label class="control-label col-sm-2" for="Nama Barang">Nama Barang :</label>
					      <div class="col-sm-10">
					        <input type="text" class="form-control" id="nama barang" placeholder="Nama Barang" name="nama_barang" value="<?php echo $dataMutasi->nama_barang?>" required autofocus>
					        <input type="hidden" name="id" value="<?php echo $dataMutasi->ID?>" required autofocus tabindex="1">
					      </div>
					    </div>
					    <div class="form-group">
					      <label class="control-label col-sm-2" for="Merk">Merk :</label>
					      <div class="col-sm-10">
					        <input type="text" class="form-control" id="merk" placeholder="Merk" name="merk" value="<?php echo $dataMutasi->merk?>" required autofocus>
					      </div>
					    </div>
					    <div class="form-group">
					      <label class="control-label col-sm-2" for="Jumlah">Jumlah :</label>
					      <div class="col-sm-10">
					        <input type="number" min="0" class="form-control" id="jumlah" placeholder="Jumlah" name="jumlah" value="<?php echo $dataMutasi->jumlah?>" required autofocus>
					      </div>
					    </div>
					    <div class="form-group">
					      <label class="control-label col-sm-2" for="NUP">NUP :</label>
					      <div class="col-sm-10">
					        <input type="text" class="form-control" id="nup" placeholder="NUP" name="nup" value="<?php echo $dataMutasi->nup?>" required autofocus>
					      </div>
					    </div>
					    <div class="form-group">
					      <label class="control-label col-sm-2" for="Dari ruangan">Dari ruangan :</label>
					      <div class="col-sm-10">
					        <input type="text" class="form-control" id="dari_ruangan" placeholder="Dari ruangan" name="dari_ruangan" value="<?php echo $dataMutasi->dari_ruangan?>" required autofocus>
					      </div>
					    </div>
					    <div class="form-group">
					      <label class="control-label col-sm-2" for="Ke ruangan">Ke ruangan :</label>
					      <div class="col-sm-10">
					        <input type="text" class="form-control" id="ke_ruangan" placeholder="Ke ruangan" name="ke_ruangan" value="<?php echo $dataMutasi->ke_ruangan?>" required autofocus>
					      </div>
					    </div>
					    <div class="form-group">
					      <label class="control-label col-sm-2" for="input-date">Tanggal :</label>
					      <div class="col-sm-10" required autofocus>
					        <div>
						      <input id="input-date" class="form-control" type="date" name="tanggal" value="<?php echo $dataMutasi->tanggal?>" required autofocus>
						      <span class="result"></span>
						  	</div>
					      </div>
					    </div>
					    <div class="form-group">
					      <label class="control-label col-sm-2" for="Keterangan">Keterangan :</label>
					      <div class="col-sm-10">
					        <input type="text" class="form-control" id="keterangan" placeholder="Keterangan" name="keterangan" value="<?php echo $dataMutasi->keterangan?>" required autofocus>
					      </div>
					    </div>
					    <div class="form-group">
						    <label class="control-label col-sm-2" for="Kategori">Kategori :</label>
							<div class="form-check">
								<label class="form-check-label">
									<input type="radio" class="form-check-input"  name="kategori" value="Medis" <?php if($dataMutasi->kategori=="Medis"){?> checked<?php }?> >
								    <span class="ml-2">Medis</span>
								</label>
							</div>
							<div class="form-check">
								<label class="form-check-label">
									<input type="radio" class="form-check-input"  name="kategori" value="Nonmedis" <?php if($dataMutasi->kategori=="Nonmedis"){?> checked<?php }?>>
								    <span class="ml-2">Non Medis</span>
								</label>
							</div>
					    </div>
					    <div class="form-group">        
					      <div class="col-sm-offset-2 col-sm-10">
					        <button type="submit" class="btn btn-default">Submit</button>
					      </div>
					    </div>
				  </form>
				</div>
			</div>
				</div>
			</div>
		</div>
		<!-- /.container-fluid -->

	</div>
	<!-- /.content-wrapper -->
	<a class="scroll-to-top rounded" href="#page-top">
		<i class="fa fa-chevron-up"></i>
	</a>

	<!-- Bootstrap core JavaScript -->
	<script src="<?php echo base_url("assets/vendor/jquery/jquery.min.js");?>"></script>
	<script src="<?php echo base_url("assets/vendor/tether/tether.min.js");?>"></script>
	<script src="<?php echo base_url("assets/vendor/bootstrap/js/bootstrap.min.js");?>"></script>

	<!-- Plugin JavaScript -->
	<script src="<?php echo base_url("vendor/jquery-easing/jquery.easing.min.js");?>"></script>
	<script src="<?php echo base_url("vendor/chart.js/Chart.min.js");?>"></script>
	<script src="<?php echo base_url("vendor/datatables/jquery.dataTables.js");?>"></script>
	<script src="<?php echo base_url("vendor/datatables/dataTables.bootstrap4.js");?>"></script>

	<!-- Custom scripts for this template -->
	<script src="<?php echo base_url("js/sb-admin.min.js");?>"></script>

</body>

</html>
